package Cast;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.mygdx.game.AngryBirdsGame;

import java.io.*;

/**
 * Created by Ali on 30/01/2016.
 */
public class GameSprite implements Serializable {
    protected GameTexture texture;
    protected Sprite sprite;
    protected boolean setInCenter = false;
    /**protected float x;
    protected float y;
    protected float rotation;
    protected float scaleX;
    protected float scaleY;
    protected float width;
    protected float height;**/
    public GameSprite(GameTexture texture){
        this.texture = texture;
        this.sprite = new Sprite(this.texture.getTexture());
    }

    /**
     * Serialize this instance.
     *
     * @param out Target to which this instance is written.
     * @throws IOException Thrown if exception occurs during serialization.
     */
    private void writeObject(final ObjectOutputStream out) throws IOException
    {
        out.writeUTF(new Float(this.getX()).toString());
        out.writeUTF(new Float(this.getY()).toString());
        out.writeUTF(new Float(this.getRotation()).toString());
        out.writeUTF(new Float(this.getScaleX()).toString());
        out.writeUTF(new Float(this.getScaleY()).toString());
        out.writeUTF(new Float(this.getWidth()).toString());
        out.writeUTF(new Float(this.getHeight()).toString());
        out.writeUTF(String.valueOf(this.setInCenter));
    }

    /**
     * Deserialize this instance from input stream.
     *
     * @param in Input Stream from which this instance is to be deserialized.
     * @throws IOException Thrown if error occurs in deserialization.
     * @throws ClassNotFoundException Thrown if expected class is not found.
     */
    private void readObject(final ObjectInputStream in) throws IOException, ClassNotFoundException
    {
        float x = Float.parseFloat(in.readUTF());
        float y = Float.parseFloat(in.readUTF());
        setRotation(Float.parseFloat(in.readUTF()));
        setScale(Float.parseFloat(in.readUTF()),Float.parseFloat(in.readUTF()));
        setSize(Float.parseFloat(in.readUTF()),Float.parseFloat(in.readUTF()));
        setInCenter = Boolean.parseBoolean(in.readUTF());

        if(setInCenter){
            setCenter(x,y);
        } else {
            setPosition(x, y);
        }

    }

    private void readObjectNoData() throws ObjectStreamException
    {
        throw new InvalidObjectException("Stream data required");
    }

    public float getX() {
        return sprite.getX();
    }


    public float getY() {
        return sprite.getY();
    }


    public float getRotation() {
        return sprite.getRotation();
    }


    public float getScaleX() {
        return sprite.getScaleX();
    }


    public float getScaleY() {
        return sprite.getScaleY();
    }


    public float getWidth() {
        return sprite.getWidth();
    }


    public float getHeight() {
        return sprite.getHeight();
    }


    public Texture getTexture() {
        return this.texture.getTexture();
    }

    public void setSize(float width, float height){
        setInCenter = false;
        sprite.setSize(width, height);
    }

    public void setScale(float scaleX, float scaleY){
        sprite.setScale(scaleX, scaleY);
    }

    public void setRotation(float degree){
        sprite.setRotation(degree);
    }

    public void setPosition(float x, float y){
        sprite.setPosition(x, y);
    }

    public void setX(float x){
        setInCenter = false;
        sprite.setX(x);
    }

    public void setY(float y){
        setInCenter = false;
        sprite.setY(y);
    }

    public void setCenterX(float centerX){
        setInCenter = true;
        sprite.setCenterX(centerX);
    }

    public void setCenterY(float centerY){
        setInCenter = true;
        sprite.setCenterY(centerY);
    }

    public void setCenter(float centerX, float centerY){
        setInCenter = true;
        setCenter(centerX, centerY);
    }
}
