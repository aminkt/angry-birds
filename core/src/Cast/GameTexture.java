package Cast;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;

import java.io.*;

/**
 * Created by Ali on 30/01/2016.
 */
public class GameTexture implements Serializable {
    String file;
    Texture texture;
    public GameTexture (String file) {
        this.file = file;
        texture = new Texture(new FileHandle(this.file));
    }

    public Texture getTexture(){
        return this.texture;
    }

    public void setTexture(String file) {
        this.file = file;
        texture = new Texture(new FileHandle(this.file));
    }

    @Override
    public String toString() {
        return super.toString();
    }

    /**
     * Serialize this instance.
     *
     * @param out Target to which this instance is written.
     * @throws IOException Thrown if exception occurs during serialization.
     */
    private void writeObject(final ObjectOutputStream out) throws IOException
    {
        out.writeUTF(this.file);
    }

    /**
     * Deserialize this instance from input stream.
     *
     * @param in Input Stream from which this instance is to be deserialized.
     * @throws IOException Thrown if error occurs in deserialization.
     * @throws ClassNotFoundException Thrown if expected class is not found.
     */
    private void readObject(final ObjectInputStream in) throws IOException, ClassNotFoundException
    {
        this.setTexture(in.readUTF());
    }

    private void readObjectNoData() throws ObjectStreamException
    {
        throw new InvalidObjectException("Stream data required");
    }
}
