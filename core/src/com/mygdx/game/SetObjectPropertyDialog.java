package com.mygdx.game;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class SetObjectPropertyDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField messInput;
    private JTextField frictionInput;
    private JButton removeObjectButton;

    public SetObjectPropertyDialog() {
        setContentPane(contentPane);
        this.setAlwaysOnTop(true);
        this.setResizable(false);
        this.setTitle("Enter Properties of object");
        setSize(200, 100);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation((int) (screenSize.getWidth()/2 - getWidth()), (int) (screenSize.getHeight()/2 -getHeight()));
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        messInput.setText(new Float(AngryBirdsGame.addedObj.getMess()).toString());
        frictionInput.setText(new Float(AngryBirdsGame.addedObj.getFriction()).toString());

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        removeObjectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onRemove();
            }
        });
// call onCancel() when cross is clicked
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

// call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
// add your code here
        if(AngryBirdsGame.addedObj!=null) {
            try {
                AngryBirdsGame.addedObj.setFriction(Float.parseFloat(frictionInput.getText()));
                AngryBirdsGame.addedObj.setMess(Float.parseFloat(messInput.getText()));
            } catch (Exception e){
                e.printStackTrace();
            }
        }
        dispose();
    }

    private void onRemove(){
        if(AngryBirdsGame.addedObj != null){
            AngryBirdsGame.holder.remove(AngryBirdsGame.addedObj);
        }
        dispose();
    }

    private void onCancel() {
// add your code here if necessary
        dispose();
    }

    @Override
    public void dispose() {
        super.dispose();
        if(AngryBirdsGame.addedObj!=null){
            AngryBirdsGame.addedObj.inChangePropertyMode = false;
            AngryBirdsGame.addedObj = null;
        }
    }
}
