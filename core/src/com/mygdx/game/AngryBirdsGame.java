package com.mygdx.game;

import AngryBirds.*;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import java.lang.reflect.InvocationTargetException;


public class AngryBirdsGame extends ApplicationAdapter implements InputProcessor {
	public static OrthographicCamera camera;
	public static SpriteBatch batch;
	public static ShapeRenderer shapeRenderer;

	float t=0;
	protected boolean pause = false;
	protected boolean touchDOwn = false;

	public static World world;
	public static Ground ground;
	public static Archery archery;

	public static Bird bird = null;
	public static GameObjectHolder holder = new GameObjectHolder();
	public static AbstractStaticObj addedObj = null;

	@Override
	public void create () {
		camera = new OrthographicCamera(Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
		camera.translate(Gdx.graphics.getWidth()/2,Gdx.graphics.getHeight()/2);
		shapeRenderer = new ShapeRenderer();
		batch = new SpriteBatch();
		Gdx.input.setInputProcessor(this);

		world = new World(-9.8f, 0.8f);
		world.addHitListener(new HitListener() {
			@Override
			public void onHit(HitEvent e) {
				//System.out.println("Hit Hot! "+e.getObj1().getClass().getSimpleName()+" | "+e.getObj2().getClass().getSimpleName());
				e.getObj2().hit(e.getObj1());
				e.getObj1().hit(e.getObj2());
			}
		});

		ground = new Ground(1.85f);
		archery = new Archery(1.5f);

		try {
			holder.load(Gdx.files.getLocalStoragePath() + "begin.ser");
			bird = (Bird) holder.get(0);
		} catch (Exception e){
			bird = new Bird(0.2f);
			bird.setPosition(
					World.convertPixelToMeter(archery.centerOfArchy.x),
					World.convertPixelToMeter(archery.centerOfArchy.y)
			);
			holder.add(bird);
			e.printStackTrace();
		}
	}

	@Override
	public void render () {
		if(!pause){
			world.hitcheck(holder);
			camera.update();
			batch.setProjectionMatrix(camera.combined);
			shapeRenderer.setProjectionMatrix(camera.combined);
			Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
			t += Gdx.graphics.getDeltaTime();
			batch.begin();
				ground.draw();
				archery.draw();

				dragUp();
				if(!bird.isInMoveMode() && bird.getHealth()==0 && bird.isReadyToShoot)
					bird.setPosition(
							World.convertPixelToMeter(archery.centerOfArchy.x-15 - World.convertMeterToPixel(bird.getRadius())),
							World.convertPixelToMeter(archery.centerOfArchy.y - World.convertMeterToPixel(bird.getRadius()))
					);

				holder.moveAll();
				holder.drawAll();
			batch.end();
			//RecordScreen.saveScreenshot();
		}
	}


	@Override
	public boolean keyDown(int keycode) {
		if(keycode == Input.Keys.S){
			holder.save();
		} else if (keycode == Input.Keys.L) {
			try {
				holder.load();
				bird = (Bird) holder.get(0);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			} catch (InstantiationException e) {
				e.printStackTrace();
			}
		} else if(keycode == Input.Keys.P){
			pause = (!pause);
		} else if(keycode == Input.Keys.R){
			bird = new Bird(0.2f);
			bird.setPosition(
					World.convertPixelToMeter(archery.centerOfArchy.x),
					World.convertPixelToMeter(archery.centerOfArchy.y)
			);
			holder.getObjects().set(0, bird);
		}
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}


	@Override
	public boolean keyTyped(char character) {
		return false;
	}


	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// Add Wall
		System.out.println(screenX+ " | "+ screenY);
		if(AddObjects.addWall(screenX, screenY, button))
			return true;


		// Pig
		if(AddObjects.addPig(screenX, screenY, button))
			return true;

		// WoodBox
		if(AddObjects.addWoodBox(screenX, screenY, button))
			return true;

		// StoneBox
		if(AddObjects.addStoneBox(screenX, screenY, button))
			return true;

		// GlassBox
		if(AddObjects.addGlassBox(screenX, screenY, button))
			return true;

		// GlassWheel
		if(AddObjects.addGlassWheel(screenX, screenY, button))
			return true;

		// StoneWheel
		if(AddObjects.addStoneWheel(screenX, screenY, button))
			return true;

		// WoodWheel
		if(AddObjects.addWoodWheel(screenX, screenY, button))
			return true;

		// Ramp
		if(AddObjects.addRamp(screenX, screenY, button))
			return true;

		if(addedObj == null)
			for (int i=0; i<holder.getSize(); i++){
				if(holder.get(i).isIntObjArea(
						World.convertPixelToMeter(screenX),
						World.convertPixelToMeter(Gdx.graphics.getHeight()-screenY)
					)<1){
					addedObj = (AbstractStaticObj) holder.get(i);
					if(button == 1){
						addedObj.isResize = true;
					} else if(button == 0){
						addedObj.isDrag = true;

					} else if(button == 2){
						addedObj.inChangePropertyMode = true;
						SetObjectPropertyDialog dialog = new SetObjectPropertyDialog();
						dialog.pack();
						dialog.setVisible(true);
					}
					if (addedObj instanceof AbstractMovableObj && button !=2){
						addedObj.setvX(0);
						addedObj.setvY(0);
					}
				}
			}
		return false;
	}


	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {

		if(addedObj!= null) {
			if(addedObj.isDrag) {
				float x = World.convertPixelToMeter(screenX);
				float y = World.convertPixelToMeter(Gdx.graphics.getHeight() - screenY);
				addedObj.setPosition(x, y);
				addedObj.isDrag = false;
				addedObj = null;
				return true;
			}

			if(addedObj.isResize){
				addedObj.isResize = false;
				addedObj = null;
				return true;
			}
		}

		return false;
	}


	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {

		if(addedObj!=null){
			if(addedObj.isDrag){
				addedObj.setPosition(
						World.convertPixelToMeter(screenX),
						World.convertPixelToMeter(Gdx.graphics.getHeight() - screenY)
				);
				return true;
			}
			if(addedObj.isResize){
					if(addedObj instanceof Rectangle){
						float width = World.convertPixelToMeter(
								screenX - addedObj.getPositionXinPixel()
						);
						float height = World.convertPixelToMeter(
								Gdx.graphics.getHeight() - screenY - addedObj.getPositionYinPixel()
						);

						float width0 = ((Rectangle) addedObj).getWidth();
						float height0 = ((Rectangle) addedObj).getHeight();
						try {
							((Rectangle) addedObj).setWidth(width);
							((Rectangle) addedObj).setHeight(height);
						} catch (Exception e){
							((Rectangle) addedObj).setWidth(width0);
							((Rectangle) addedObj).setHeight(height0);
							System.err.println("Size: "+width+" | "+height);
							e.printStackTrace();
						}
					}
					else if(addedObj instanceof Cycle){
						float radius = World.convertPixelToMeter(screenX-addedObj.getPositionXinPixel()/2);
						float radius0  = ((Cycle) addedObj).getRadius();
						try {
							((Cycle) addedObj).setRadius(radius);
						} catch (Exception e){
							((Cycle) addedObj).setRadius(radius0);
							System.err.println("radius: "+radius);
							e.printStackTrace();
						}
					}
				return false;
			}
		}
		return false;
	}


	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}


	@Override
	public boolean scrolled(int amount) {
		/*if(camera.zoom+0.02*amount > 1){
			camera.zoom = 1;
		}
		else{
			camera.zoom += 0.02*amount;
		}*/
		return true;
	}

	public boolean dragUp(){
		if(Gdx.input.isTouched()){
			touchDOwn = true;
			//System.out.println("Touch Down");
		} else {
			if(touchDOwn){
				int b = 0;
				if(Gdx.input.isButtonPressed(1))
					b = 1;
				else if(Gdx.input.isButtonPressed(2))
					b = 2;
				onDragUp(Gdx.input.getX(), Gdx.input.getY(), b);
			}
			touchDOwn = false;
			//System.out.println("Touch Up");
		}
		return false;
	}

	public boolean onDragUp(int screenX, int screenY, int button){
		if(archery.inShootRange(screenX, Gdx.graphics.getHeight()-screenY)){
			if(!bird.isInMoveMode() && bird.isReadyToShoot){
				float deltaX = World.convertPixelToMeter(archery.center.x - screenX);
				float deltaY =   World.convertPixelToMeter(archery.center.y - Gdx.graphics.getHeight()+screenY);
				double xForce = World.K_OF_ARCHERY*deltaX;
				double yForce = World.K_OF_ARCHERY*deltaY;
				//System.err.println(xForce +" | "+ yForce);
				bird.isReadyToShoot = false;
				bird.setForce(xForce, yForce);
				//bird.setXForce(xForce);
				//System.out.println(bird.getVX());
				//System.out.println("shoot");
			}
			else {
				System.out.println("continue moving");
			}
		}
		return false;
	}
}
