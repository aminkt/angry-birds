package com.mygdx.game;

import AngryBirds.*;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Ali on 28/01/2016.
 *
 * This class hold all object in itself.
 */
public class GameObjectHolder implements Serializable {
    protected ArrayList<Contactable> objects;
    protected ArrayList<Contactable> avilableObjectList;

    GameObjectHolder() {
        objects = new ArrayList<Contactable>();
        avilableObjectList = new ArrayList<Contactable>();
    }

    public ArrayList<Contactable> getObjects() {
        return this.objects;
    }

    public void add(Contactable obj) {
        if (obj == null)
            throw new IllegalArgumentException("Null object can not add to GameObjectHolder container.");
        objects.add(obj);
    }

    public void remove(Contactable obj) {
        if (obj == null)
            throw new IllegalArgumentException("Null object can not add to GameObjectHolder container.");
        objects.remove(obj);
    }

    public void drawAll() {
        for (int i = 0; i < objects.size(); i++) {
            objects.get(i).draw();
        }
    }

    public void moveAll() {
        for (int i = 0; i < objects.size(); i++) {
            if(objects.get(i) instanceof AbstractMovableObj){
                ((AbstractMovableObj) objects.get(i)).move();
            }

        }
    }

    public Contactable get(int index) {
        return objects.get(index);
    }

    public Contactable find(Contactable obj) {
        return objects.get(objects.indexOf(obj));
    }

    public int getSize() {
        return objects.size();
    }

    @Override
    public String toString() {
        super.toString();
        return new StringBuffer(" Objects : ")
                .append(this.objects.toString()).toString();
    }

    public void save() {
        ArrayList<HashMap> s = new ArrayList<HashMap>();
        HashMap<String, String> temp;
        for (int i = 0; i < getSize(); i++) {
            temp = new HashMap<String, String>();
            temp.put("Class", get(i).getClass().getSimpleName());
            if (get(i) instanceof AbstractStaticObj) {
                temp.put("Mess", new Float(((AbstractStaticObj) get(i)).getMess()).toString());
                temp.put("Friction", new Float(((AbstractStaticObj) get(i)).getFriction()).toString());
                temp.put("X", new Float(((AbstractStaticObj) get(i)).getPositionX()).toString());
                temp.put("Y", new Float(((AbstractStaticObj) get(i)).getPositionY()).toString());
                temp.put("Rotation", new Float(((AbstractStaticObj) get(i)).getSprite().getRotation()).toString());
                if (get(i) instanceof Rectangle) {
                    temp.put("Width", new Float(((Rectangle) get(i)).getWidth()).toString());
                    temp.put("Height", new Float(((Rectangle) get(i)).getHeight()).toString());
                    temp.put("Shape", Rectangle.class.getSimpleName());
                } else if (get(i) instanceof Cycle) {
                    temp.put("Radius", new Float(((Cycle) get(i)).getRadius()).toString());
                    temp.put("Shape", Cycle.class.getSimpleName());
                } else if (get(i) instanceof Triangle) {
                    temp.put("Side", new Float(((Triangle) get(i)).getSide()).toString());
                    temp.put("Shape", Triangle.class.getSimpleName());
                }
            } else {
                throw new IndexOutOfBoundsException("Saved Object is not an AbstractStaticObject.");
            }
            s.add(temp);
        }
        Serialization.serialize(s);
    }

    public void load(Object o) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {

        if (o instanceof ArrayList) {
            ArrayList<HashMap> s = (ArrayList<HashMap>) o;
            ArrayList<Contactable> objects = new ArrayList<Contactable>();
            for (int i = 0; i < s.size(); i++) {
                AbstractStaticObj obj = null;
                String className = (String) s.get(i).get("Class");
                if (className.equals(Bird.class.getSimpleName())) {
                    Float r = Float.parseFloat((String) s.get(i).get("Radius"));
                    obj = new Bird(r);
                } else if (className.equals(Pig.class.getSimpleName())) {
                    Float r = Float.parseFloat((String) s.get(i).get("Radius"));
                    obj = new Pig(r);
                } else if (className.equals(StoneWheel.class.getSimpleName())) {
                    Float r = Float.parseFloat((String) s.get(i).get("Radius"));
                    obj = new StoneWheel(r);
                } else if (className.equals(WoodWheel.class.getSimpleName())) {
                    Float r = Float.parseFloat((String) s.get(i).get("Radius"));
                    obj = new WoodWheel(r);
                } else if (className.equals(GlassWheel.class.getSimpleName())) {
                    Float r = Float.parseFloat((String) s.get(i).get("Radius"));
                    obj = new GlassWheel(r);
                } else if (className.equals(Ramp.class.getSimpleName())) {
                    Float side = Float.parseFloat((String) s.get(i).get("Side"));
                    obj = new Ramp(side);
                }
                if (className.equals(Wall.class.getSimpleName())) {
                    Float w = Float.parseFloat((String) s.get(i).get("Width"));
                    Float h = Float.parseFloat((String) s.get(i).get("Height"));
                    obj = new Wall(w, h);
                } else if (className.equals(StoneBox.class.getSimpleName())) {
                    Float w = Float.parseFloat((String) s.get(i).get("Width"));
                    Float h = Float.parseFloat((String) s.get(i).get("Height"));
                    obj = new StoneBox(w, h);
                } else if (className.equals(GlassBox.class.getSimpleName())) {
                    Float w = Float.parseFloat((String) s.get(i).get("Width"));
                    Float h = Float.parseFloat((String) s.get(i).get("Height"));
                    obj = new GlassBox(w, h);
                } else if (className.equals(WoodBox.class.getSimpleName())) {
                    Float w = Float.parseFloat((String) s.get(i).get("Width"));
                    Float h = Float.parseFloat((String) s.get(i).get("Height"));
                    obj = new WoodBox(w, h);
                }
                obj.setPosition(Float.parseFloat((String) s.get(i).get("X")), Float.parseFloat((String) s.get(i).get("Y")));
                obj.setMess(Float.parseFloat((String) s.get(i).get("Mess")));
                obj.setFriction(Float.parseFloat((String) s.get(i).get("Friction")));
                obj.getSprite().setRotation(Float.parseFloat((String) s.get(i).get("Rotation")));
                objects.add(obj);
            }
            this.objects = objects;
            System.out.println(s.toString());
        } else {
            throw new IllegalArgumentException("Save File not save correctly");
        }
    }

    public void load(String path) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Object o = Serialization.deserialize(path);
        load(o);
    }

    public void load() throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Object o = Serialization.deserialize();
        load(o);
    }
}
