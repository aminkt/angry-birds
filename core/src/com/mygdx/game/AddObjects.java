package com.mygdx.game;

import AngryBirds.*;
import com.badlogic.gdx.Gdx;

/**
 * Created by Ali on 29/01/2016.
 */
public class AddObjects {

    public static boolean addWoodWheel(float screenX, float screenY, int b){
        if (screenX > 60 && screenY>11 && screenX<130 && screenY<69 && b==1){
            AngryBirdsGame.addedObj = new WoodWheel(0.5f);
            addObjectToHolder(screenX, screenY);
            return true;
        }
        return false;
    }

    public static boolean addStoneWheel(float screenX, float screenY, int b){
        if (screenX > 145 && screenY>10 && screenX<226 && screenY<66 && b==1){
            AngryBirdsGame.addedObj = new StoneWheel(0.5f);
            addObjectToHolder(screenX, screenY);
            return true;
        }
        return false;
    }

    public static boolean addGlassWheel(float screenX, float screenY, int b){
        if (screenX > 60 && screenY>101 && screenX<130 && screenY<165 && b==1){
            AngryBirdsGame.addedObj = new GlassWheel(0.5f);
            addObjectToHolder(screenX, screenY);
            return true;
        }
        return false;
    }

    public static boolean addGlassBox(float screenX, float screenY, int b){
        if (screenX > 10 && screenY>15 && screenX<45 && screenY<52 && b==1){
            AngryBirdsGame.addedObj = new GlassBox(0.5f, 0.5f);
            addObjectToHolder(screenX, screenY);
            return true;
        }
        return false;
    }

    public static boolean addStoneBox(float screenX, float screenY, int b){
        if (screenX > 10 && screenY>69 && screenX<45 && screenY<103 && b==1){
            AngryBirdsGame.addedObj = new StoneBox(0.5f, 0.5f);
            addObjectToHolder(screenX, screenY);
            return true;
        }
        return false;
    }

    public static boolean addWoodBox(float screenX, float screenY, int b){
        if (screenX > 10 && screenY>120 && screenX<45 && screenY<157 && b==1){
            AngryBirdsGame.addedObj = new WoodBox(0.5f, 0.5f);
            addObjectToHolder(screenX, screenY);
            return true;
        }
        return false;
    }

    public static boolean addPig(float screenX, float screenY, int b){
        if (screenX > 148 && screenY>95 && screenX<220 && screenY<160 && b==1){
            AngryBirdsGame.addedObj = new Pig(0.25f);
            addObjectToHolder(screenX, screenY);
            return true;
        }
        return false;
    }

    public static boolean addWall(float screenX, float screenY, int b){
        if (screenX > 242 && screenY>14 && screenX<300 && screenY<152 && b==1){
            AngryBirdsGame.addedObj = new Wall(0.5f,2);
            addObjectToHolder(screenX, screenY);
            return true;
        }
        return false;
    }


    public static void addBirdToShootMode(){

    }
    public static boolean addRamp(float screenX, float screenY, int b){
        return false;
    }

    private static void addObjectToHolder(float screenX, float screenY){
        AngryBirdsGame.addedObj.isDrag = true;
        AngryBirdsGame.addedObj.setPosition(World.convertPixelToMeter(screenX), World.convertPixelToMeter(Gdx.graphics.getHeight() - screenY));
        AngryBirdsGame.holder.add(AngryBirdsGame.addedObj);
        AngryBirdsGame.addedObj = (AbstractStaticObj) AngryBirdsGame.holder.find(AngryBirdsGame.addedObj);
    }
}
