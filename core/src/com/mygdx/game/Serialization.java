package com.mygdx.game;

import com.badlogic.gdx.Gdx;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Created by Ali on 30/01/2016.
 */
public class Serialization {

    public static void serialize(Object o){
        try{
            String path = Gdx.files.getLocalStoragePath() + "holder" + ".ser";
            FileOutputStream fout = new FileOutputStream(path);
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(o);
            oos.close();
            System.out.println("Save done!");

        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    public static Object deserialize(String path){
        try {
            // Read from disk using FileInputStream
            FileInputStream f_in = new FileInputStream(path);

            // Read object using ObjectInputStream
            ObjectInputStream obj_in = new ObjectInputStream (f_in);

            System.out.println("Load done!");
            // Read an object
            return  obj_in.readObject();

        } catch (Exception e){
            e.printStackTrace();
        }

        return null;
    }

    public static Object deserialize(){
        String path = Gdx.files.getLocalStoragePath() + "holder.ser";
        return deserialize(path);
    }

}
