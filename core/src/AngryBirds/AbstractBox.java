package AngryBirds;

import Cast.GameSprite;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.mygdx.game.AngryBirdsGame;

/**
 * Created by Ali on 23/01/2016.
 */
public abstract class AbstractBox extends AbstractMovableObj implements Rectangle {
    protected float width;
    protected float height;


    /**
     * For reduce retyping code of child class init method, work same this abstract constructor.
     * @param width float
     * @param height float
     */
    public void init(float width, float height){
        setSprit(new GameSprite(getTexture()));
        setWidth(width);
        setHeight(height);
        //getSprite().setSize(getWidth(), getHeight());
    }


    /**
     * <p><strong>Controlling hit action</strong></p>
     * <p>This method get an object and define property of object that has collision whit the current object
     * and change property of both object same forces and health of them.</p>
     * <p>Objects can be static object or movable object.</p>
     *
     * @param obj AbstractStaticObj
     */
    @Override
    public void hit(AbstractStaticObj obj) {
        super.hit(obj);
    }

    /**
     * Set width of rectangle.
     *
     * @param width float
     */
    @Override
    public void setWidth(float width) {
        if (width<=0)
            throw new IllegalArgumentException("Width of box can not be zero or negative number.");
        this.width = width;
        getSprite().setSize(getWidth(),getHeight());
    }

    /**
     * set height of rectangle
     *
     * @param height float
     */
    @Override
    public void setHeight(float height) {
        if (height<=0)
            throw new IllegalArgumentException("Height of box can not be zero or negative number.");
        this.height = height;
        getSprite().setSize(getWidth(),getHeight());
    }

    /**
     * Set height and width of rectangle.
     *
     * @param width  float
     * @param height float
     */
    @Override
    public void setAsBox(float width, float height) {
        setHeight(height);
        setWidth(width);
    }

    /**
     * Return width of rectangle
     *
     * @return width float
     */
    @Override
    public float getWidth() {
        return this.width;
    }

    /**
     * Return height of rectangle
     *
     * @return height float
     */
    @Override
    public float getHeight() {
        return this.height;
    }

    /**
     * Drow object in screen.
     */
    @Override
    public void draw() {
        Sprite s = World.setMeterSpritToPixel(getSprite());
        AngryBirdsGame.batch.draw(s, s.getX(), s.getY(), s.getWidth(), s.getHeight());
    }

    /**
     * Get an place in x-y of screen and return an int nubmer. <br> 1 = if x-y point is out of obj<br> 0 = if on obj<br> -1 = if in obj
     *
     * @param x
     * @param y
     * @return int
     */
    @Override
    public int isIntObjArea(float x, float y) {
        if (getSprite().getX()<= x && x <= getSprite().getX()+getWidth() && getSprite().getY()<= y && y <= getSprite().getY()+getHeight() )
        {
            if (x==getSprite().getX() || x==getSprite().getX()+getWidth() || y==getSprite().getY() || y==getSprite().getY()+getHeight() )
                return 0;
            else
                return -1;
        }
        return 1;
    }


    /**
     * check for know a hit happen between this shape and a cycle shape.
     * @param cycle
     * @return int - 0=no / 1=yes
     */
    public int checkHit(Cycle cycle)
    {
        return World.checkHit_CR(cycle.getCenterX(), cycle.getCenterY(), cycle.getRadius(), getPositionX(), getPositionY(), getWidth(), getHeight());
    }

    /**
     * check for know a hit happen between this shape and a triangle shape.
     * @param triangle
     * @return int - 0=no / 1=yes
     */
    public int checkHit(Triangle triangle)
    {
        return World.checkHit_TR(triangle.getPositionX(), triangle.getPositionY(), triangle.getSide(), getPositionX(), getPositionY(), getWidth(), getHeight());
    }

    /**
     * check for know a hit happen between this shape and a rectangle shape.
     * @param rectangle
     * @return int - 0=no / 1=yes
     */
    public int checkHit(Rectangle rectangle){
        return World.checkHit_RR(getPositionX(), getPositionY(), getWidth(), getHeight(), rectangle.getPositionX(), rectangle.getPositionY(), rectangle.getWidth(), rectangle.getHeight());
    }
}
