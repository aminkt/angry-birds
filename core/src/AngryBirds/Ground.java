package AngryBirds;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.mygdx.game.AngryBirdsGame;

/**
 * Created by Ali on 20/01/2016.
 *
 * This class define ground behavior for do good reaction to objects in game.
 * ground is an static obj and don't move by force but same as other object has its physics.
 */
public class Ground extends AbstractStaticObj {
    protected static float heightOfGround;

    public Ground(float heightOfGround) {
        Ground.heightOfGround = heightOfGround;
        this.setFriction(World.GROUND_DEFUALT_FRICTION);
        this.setRestitution(World.GROUND_DEFUALT_RESTITUTION);
        this.mess = 999999999;
        setTexture("5.png");
        setPosition(0,0);
    }

    /**
     * Set height of ground for controlling objects.
     * @param heigh float
     */
    public static void setHeightOfGround(float heigh){
        if (heigh<0)
            throw new IllegalArgumentException("Height of ground can not be negative.");
        Ground.heightOfGround = heigh;
    }

    public static float getHightOfGround(){
        return Ground.heightOfGround;
    }

    /**
     * Drow object in screen.
     */
    public void draw() {
        AngryBirdsGame.batch.draw(getTexture().getTexture(),0,0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    }

    /**
     * Get an place in x-y of screen and return an int nubmer. <br> 1 = if x-y point is out of obj<br> 0 = if on obj<br> -1 = if in obj
     *
     * @param x
     * @param y
     * @return int
     */
    @Override
    public int isIntObjArea(float x, float y) {
        return 1;
    }

    /**
     * <p><strong>Controlling hit action</strong></p>
     * <p>This method get an object and define property of object that has collision whit the current object
     * and change property of both object same forces and health of them.</p>
     * <p>Objects can be static object or movable object.</p>
     *
     * @param obj AbstractStaticObj
     */
    @Override
    public void hit(AbstractStaticObj obj) {

    }

    /**
     * check for know a hit happen between this shape and a cycle shape.
     *
     * @param cycle
     * @return int - 0=no / 1=yes
     */
    @Override
    public int checkHit(Cycle cycle) {
        if(cycle.getPositionX() <= heightOfGround)
            return 1;
        return 0;
    }

    /**
     * check for know a hit happen between this shape and a triangle shape.
     *
     * @param triangle
     * @return int - 0=no / 1=yes
     */
    @Override
    public int checkHit(Triangle triangle) {
        if(triangle.getPositionX() <= heightOfGround)
            return 1;
        return 0;
    }

    /**
     * check for know a hit happen between this shape and a rectangle shape.
     *
     * @param rectangle
     * @return int - 0=no / 1=yes
     */
    @Override
    public int checkHit(Rectangle rectangle) {
        if(rectangle.getPositionX() <= heightOfGround)
            return 1;
        return 0;
    }
}
