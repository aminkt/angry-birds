package AngryBirds;

import com.badlogic.gdx.graphics.g2d.Sprite;
import Cast.GameSprite;
import com.mygdx.game.GameObjectHolder;

import java.awt.image.PixelGrabber;
import java.util.ArrayList;

/**
 * Created by Ali on 20/01/2016.
 *
 * This class define some important constant and variables for define physics world and prepare an Event Listener.
 */
public class World {
    public static float K_OF_ARCHERY = 70f;

    /**
     * Define some default values for game object's friction.
     */
    public static float GROUND_DEFUALT_FRICTION = 3.5f;
    public static float STONE_DEFUALT_FRICTION = 0.9f;
    public static float GLASS_DEFUALT_FRICTION = 0.2f;
    public static float WOOD_DEFUALT_FRICTION = 0.7f;
    public static float BIRD_DEFUALT_FRICTION = 0.5f;
    public static float PIG_DEFUALT_FRICTION = 0.4f;

    /**
     * Define some default values for game object's Mess.
     */
    public static float GROUND_DEFUALT_MESS = 10f;
    public static float STONE_DEFUALT_MESS = 1.5f;
    public static float GLASS_DEFUALT_MESS = 0.8f;
    public static float WOOD_DEFUALT_MESS = 1f;
    public static float BIRD_DEFUALT_MESS = 0.5f;
    public static float PIG_DEFUALT_MESS = 0.5f;

    /**
     * Define some default values for game object's friction.
     */
    public static float GROUND_DEFUALT_RESTITUTION = 0.6f;
    public static float STONE_DEFUALT_RESTITUTION = 0.8f;
    public static float GLASS_DEFUALT_RESTITUTION = 0.25f;
    public static float WOOD_DEFUALT_RESTITUTION = 0.4f;
    public static float BIRD_DEFUALT_RESTITUTION = 0.7f;
    public static float PIG_DEFUALT_RESTITUTION = 0.18f;

    /**
     * Define some default values for game object's hardness factor.
     */
    public static float GROUND_DEFUALT_HARDNESS = 1000;
    public static float STONE_DEFUALT_HARDNESS = 20;
    public static float GLASS_DEFUALT_HARDNESS = 8f;
    public static float WOOD_DEFUALT_HARDNESS = 14f;
    public static float BIRD_DEFUALT_HARDNESS = 1000;
    public static float PIG_DEFUALT_HARDNESS = 5f;

    /**
     * Use for change Pixel to meter and meter to pixel for show object in game screen.
     */
    protected static final int METER_TO_PIX = 100;


    /**
     * Define gravity in game.
     */
    public static float gravity;

    /**
     * Time factor definition for controlling speed of game.
     */
    protected static float timeFactor;


    /**
     * List of listiner added to world.
     */
    private ArrayList<HitListener> hitListenersList = new ArrayList<HitListener>();

    /**
     * Add hit listener to world
     * @param listener HitListener
     */
    public synchronized void addHitListener(HitListener listener){
        if(!hitListenersList.contains(listener))
            hitListenersList.add(listener);
    }

    /**
     * This method call every time for checking new hit event and if occurred then throw an event.
     * @param objects
     */
    public void hitcheck(GameObjectHolder objects){
        if (objects.getObjects().isEmpty())
            return;

        for(int i=0; i<objects.getSize(); i++){
            for (int j=i+1; j<objects.getSize(); j++){
                if(objects.get(i).checkContact((AbstractStaticObj) objects.get(j)))
                    processHitEvent(new HitEvent(this, (AbstractStaticObj) objects.get(i), (AbstractStaticObj) objects.get(j)));
            }
        }

    }

    /**
     * Process hit that was throw by hitcheck method.
     * @param hitEvent
     */
    private void processHitEvent(HitEvent hitEvent){
        ArrayList<HitListener> tmpHitListenersList;
        synchronized (this){
            if (hitListenersList.size()==0)
                return;
            tmpHitListenersList = (ArrayList<HitListener>) hitListenersList.clone();
        }
        for(HitListener listener : tmpHitListenersList){
            listener.onHit(hitEvent);
        }
    }



        public World(){
        World.gravity = -9.8f;
        World.timeFactor = 1;
    }
    public World(float gravity){
        World.gravity = gravity;
        World.timeFactor = 1;
    }
    public World(float gravity, float timeFactor) {
        World.gravity = gravity;
        World.timeFactor = timeFactor;
    }


    /**
     * Return time factor for controlling some action in game like speed of object movement.
     * @return timeFactor - float
     */
    public static float getTimeFactor() {
        return timeFactor;
    }

    /**
     * Set time factor in game for change speed of game.
     * @param timeFactor float
     */
    public static void setTimeFactor(float timeFactor) {
        if (timeFactor <= 0)
            throw new IllegalArgumentException("Setting time factor to zero or negative number cause problem.");
        World.timeFactor = timeFactor;
    }

    public static float convertMeterToPixel(float meter){
        return meter*World.METER_TO_PIX;
    }

    public static float convertPixelToMeter(float pixel){
        return pixel/METER_TO_PIX;
    }

    public static Sprite setMeterSpritToPixel(GameSprite s){
        Sprite n = new Sprite(s.getTexture());
        n.setPosition(World.convertMeterToPixel(s.getX()), World.convertMeterToPixel(s.getY()));
        //n.setCenterX(World.convertMeterToPixel(s.getX()));
        //n.setCenterY(World.convertMeterToPixel(s.getY()));
        n.setRotation(s.getRotation());
        n.setScale(s.getScaleX(), n.getScaleY());
        n.setSize(World.convertMeterToPixel(s.getWidth()), World.convertMeterToPixel(s.getHeight()));
        return n;
    }




    static int checkinCircle(double xc, double yc, double r, double x, double y)
    {
        //INPUTS:   (xc,yc) mokhtasate markaz, r shoae, (x,y) checkpoint
        //OUTPUT:   1: biroon   0:roo khat  -1:dakhel
        if ((x-xc)*(x-xc) + (y-yc)*(y-yc)==r*r){
            return 0;
        }
        else if ((x-xc)*(x-xc) + (y-yc)*(y-yc)<r*r){
            return -1;
        }
        else
            return 1;
    }

    static int checkinRectangular(double xr , double yr, double w, double h, double x, double y)
    {
        //INPUT:    (xr,yr) mokhtasate point chap paeen, w arz , h ertefa, (x,y) checkpoint
        //OUTPUT:   1: biroon   0:roo khat  -1:dakhel
        if (xr<= x && x <= xr+w && yr<= y && y <= yr+h )
        {
            if (x==xr || x==xr+w || y==yr || y==yr+h )
                return 0;
            else
                return -1;
        }
        else
            return 1;
    }
    // مثلث قائم الزاویه با زاویه چهل و پنج و ضلع آ
    static int checkinTriangle(double xt, double yt, double a , double x, double y)
    {
        //INPUT:    (xt, yt) mokhtasate gosheye chap paeen, a ضلع, (x,y) checkpoint
        //OUTPUT:   1: biroon   0:roo khat  -1:dakhel
        double b= Math.sqrt(3);

        if  (checkinRectangular(xt, yt , a, a , x , y)<=0)
        {
            if (y == x+ yt- xt || y==yt || x==xt+a)
                return 0;
            else if (y<x+yt-xt)
                return -1;
            else
                return 1;
        }
        else
            return 1;
    }

    static int checkHit_CT(double xc, double yc, double r, double xt, double yt, double a )
    {
        //DAYERE:   (xc,yc) mokhtasate markaz, r shoae
        //MOSALAS:  (xt, yt) mokhtasate goosheye chap paeen, a ضلع

        double b , x1 , x2, x3, x4, y1, y2, y3, y4;
        b= yt-xt- yc;
        x1= ((xc-b)+ Math.sqrt(2*r*r - (b+xc)*(b+xc)))/2;
        x2= ((xc-b)- Math.sqrt(2*r*r - (b+xc)*(b+xc)))/2;
        x3= xc + Math.sqrt(r*r - (yt-yc)*(yt-yc));
        x4= xc - Math.sqrt(r*r - (yt-yc)*(yt-yc));
        y1= x1+ yt- xt;
        y2= x2+ yt- xt;
        y3= yc+ Math.sqrt(r*r - (xt+a-xc)*(xt+a-xc));
        y4= yc- Math.sqrt(r*r - (xt+a-xc)*(xt+a-xc));

        if (checkinCircle(xc, yc, r, x1, y1)==0 && checkinTriangle(xt, yt, a, x1, y1)==0)
            return 1;
        else if (checkinCircle(xc, yc, r, x2, y2)==0 && checkinTriangle(xt, yt, a, x2, y2)==0)
            return 1;
        else if (checkinCircle(xc, yc, r, x3, yt)==0 && checkinTriangle(xt, yt, a, x3, yt)==0)
            return 1;
        else if (checkinCircle(xc, yc, r, x4, yt)==0 && checkinTriangle(xt, yt, a, x4, yt)==0)
            return 1;
        else if (checkinCircle(xc, yc, r, xt+a, y3)==0 && checkinTriangle(xt, yt, a, xt+a, y3)==0)
            return 1;
        else if (checkinCircle(xc, yc, r, xt+a, y4)==0 && checkinTriangle(xt, yt, a, xt+a, y4)==0)
            return 1;
        else
            return 0;
    }

    static int checkHit_TR(double xt, double yt, double a ,double xr, double yr, double w, double h)
    {
        //MOSALAS:      (xt, yt) mokhtasate gosheye chap paeen, a ضلع
        // MOSTATIL:    (xr,yr) mokhtasate point chap paeen , w arz , h ertefa
        double x1 , y1;
        y1= xr-xt+yt;
        x1= yr+xt-yt;

        if (checkinTriangle(xt, yt, a, xr, y1)==0 && checkinRectangular(xr, yr, w, h, xr, y1)==0)
            return 1;

        else if (checkinTriangle(xt, yt, a, xr+w, y1+w)==0 && checkinRectangular(xr, yr, w, h, xr+w, y1+w)==0)
            return 1;

        else if (checkinTriangle(xt, yt, a, x1, yr)==0 && checkinRectangular(xr, yr, w, h, x1, yr)==0)
            return 1;

        else if (checkinTriangle(xt, yt, a, x1+h, yr+h)==0 && checkinRectangular(xr, yr, w, h, x1+h, yr+h)==0)
            return 1;

        else if (checkinTriangle(xt, yt, a, xr, yt)==0 && checkinRectangular(xr, yr, w, h, xr, yt)==0)
            return 1;

        else if (checkinTriangle(xt, yt, a, xr+w, yt)==0 && checkinRectangular(xr, yr, w, h, xr+w, yt)==0)
            return 1;

        else if (checkinTriangle(xt, yt, a, xt+a, yr)==0 && checkinRectangular(xr, yr, w, h, xt+a, yr)==0)
            return 1;

        else if (checkinTriangle(xt, yt, a, xt+a, yr+h)==0 && checkinRectangular(xr, yr, w, h, xt+a, yr+h)==0)
            return 1;

        else if (yt==yr && ((xr<= xt+a && xt+a<= xr +w) || (xr<= xt && xt<= xr+w)))
            return 1;

        else if (yt==yr+h && ((xr<= xt+a && xt+a<= xr +w) || (xr<= xt && xt<= xr+w)))
            return 1;

        else if (xt+a==xr && ((yr<= yt+a && yt+a<= yr +h) || (yr<= yt && yt<= yr+h)))
            return 1;

        else if (xt+a==xr+w && ((yr<= yt+a && yt+a<= yr +h) || (yr<= yt && yt<= yr+h)))
            return 1;

        else return 0;
    }

    static int checkHit_CR(double xc, double yc, double r, double xr, double yr , double w, double h)
    {
        //DAYERE:   (xc,yc) mokhtasate markaz, r shoae
        //MOSTATIL: (xr,yr) mokhtasate point chap paeen, w arz , h ertefa
        if( yr<=yc && yc<=yr+h && xr-r<= xc && xc<=yr+w+r)
            return 1;
        else if(yr<=yc && yc<=yr+h+r && xr<=xc && xc<=xr+w)
            return 1;
        else if (checkinCircle(xc, yc, r, xr, yr)<=0)
            return 1;
        else if (checkinCircle(xc, yc, r, xr, yr+h)<=0)
            return 1;
        else if (checkinCircle(xc, yc, r, xr+w, yr+h)<=0)
            return 1;
        else if (checkinCircle(xc, yc, r, xr+w, yr)<=0)
            return 1;
        else return 0;
    }

    static int checkHit_CC(double xc1, double yc1, double r1, double xc2, double yc2, double r2)
    {
        if (checkinCircle(xc2, yc2, r1+r2, xc1, yc1)<=0)
            return 1;
        else return 0;
    }

    static int checkHit_RR(double xr1, double yr1 , double w1, double h1, double xr2, double yr2 , double w2, double h2 )
    {
        if(checkinRectangular(xr2-w1, yr2-h1, w1+w2, h1+h2, xr1, yr1)<=0)
            return 1;
        else return 0;
    }

    static int checkinTriangle__balae(double xt, double yt, double a , double x, double y)
    {
        //INPUT:    (xt, yt) mokhtasate gosheye chap paeen, a ضلع, (x,y) checkpoint
        //OUTPUT:   1: biroon   0:roo khat  -1:dakhel
        double b= Math.sqrt(3);

        if  (checkinRectangular(xt, yt , a, a , x , y)<=0)
        {
            if (y == x+ yt- xt || y==yt+a || x==xt)
                return 0;
            else if (y>x+yt-xt)
                return -1;
            else
                return 1;
        }
        else
            return 1;
    }

    static int checkHit_TT(double xt1, double yt1, double a1, double xt2, double yt2, double a2 )
    {
        if(checkinTriangle(xt2-a1, yt2, a2, xt1, yt1 )<=0)
            return 1;
        else if(checkinRectangular(xt2+a2-a1, yt2, a1, a2, xt1, yt1)<=0)
            return 1;
        else if(checkinRectangular(xt2-a1, yt2-a1, a2, a1, xt1, yt1)<=0)
            return 1;
        else if(checkinTriangle__balae(xt2+a2-a1, yt2-a1, a1, xt1, yt1)<=0 )
            return 1;
        else return 0;
    }
}
