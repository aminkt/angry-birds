package AngryBirds;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import Cast.GameSprite;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.mygdx.game.AngryBirdsGame;

/**
 * Created by Ali on 21/01/2016.
 *
 * Describe Bird Object Property.
 */
public class Bird extends AbstractMovableObj implements Cycle{

    public boolean isReadyToShoot = true;
    protected float radius;
    //protected boolean hitProccess = false;
    public Bird(float radius) {
        setFriction(World.BIRD_DEFUALT_FRICTION);

        setTexture("bird1.png");
        super.setSprit(new GameSprite(getTexture()));
        setRadius(radius);
        setMess(World.BIRD_DEFUALT_MESS*(radius));
        setHardnessFactor(World.BIRD_DEFUALT_HARDNESS);
        setRestitution(World.BIRD_DEFUALT_RESTITUTION);


    }

    @Override
    public void setYForce(double yForce){
        super.setYForce(yForce);
        float x = World.convertMeterToPixel(getCenterX()-0.5f);
        float x2 = World.convertMeterToPixel(getCenterX());
        float y = World.convertMeterToPixel(getCenterY());
        if(isReadyToShoot && (AngryBirdsGame.archery.inShootRange(x, y) || AngryBirdsGame.archery.inShootRange(x2, y)))
            setvY(0);
    }

    @Override
    public synchronized boolean isInMoveMode() {
        if(getVX() == 0){
            isReadyToShoot = true;
            return false;
        }
        return true;
    }

    /**
     * <p><strong>Controlling hit action</strong></p>
     * <p>This method get an object and define property of object that has collision whit the current object
     * and change property of both object same forces and health of them.</p>
     * <p>Objects can be static object or movable object.</p>
     *
     * @param obj AbstractStaticObj
     */
    @Override
    public synchronized void hit(AbstractStaticObj obj) {
        System.out.println(getVX()+" | "+getVY());
        if(obj.isStatic()){
            setvX(getVX()*getRestitution());
            setvY(getVY());
            setForce(0.01f, 0);
            System.err.println(getVX()+" | "+getVY());
        }
        else {
            super.hit(obj);
        }

    }

    /**
     * <p><strong>Set y force together</strong></p>
     *
     * @param xForce double
     */
    public synchronized void setXForce(double xForce){
        double fs;
        double time = Gdx.graphics.getDeltaTime()*World.timeFactor;
        float v;
        if(getPositionY()<= Ground.heightOfGround)
            fs = (getMess()*World.gravity* AngryBirdsGame.ground.getFriction());
        else
            fs = 0;

        if(xForce>=0)
        {
            v= (float) (((xForce+fs)*time)/getMess() +this.vX);
            if((xForce==0 || xForce<fs) && this.vX==0)
                v=0;
            if (xForce==0 && this.vX<=0 && getPositionY()<= Ground.heightOfGround)
                v=0;
        }
        else{
            v= (float) (((xForce+fs)*time)/getMess() +this.vX);
        }
        setvX(v);
    }

    @Override
    public synchronized void move() {
        super.move();
        float d = getRotate();
        setRotate(-getVX()+getVY()+d);
    }

    /**
     * <p><strong>Change shape and picture of object by them health.</strong></p>
     * <p>For example when an wood box is healthy show prefect picture for that and when is not healthy change
     * that picture to a broken box.</p>
     *
     * @param health int
     */
    @Override
    protected void setFaceType(int health) {
        return;
    }

    /**
     * <p><strong>Return a int number for describe current face type. </strong></p>
     * <p>This number is between 1 and 5.</p>
     *
     * @param health int
     * @return faceType constant int
     */
    @Override
    protected int getFaceType(int health) {
        return 0;
    }

    /**
     * Get an place in x-y of screen and return an int nubmer. <br> 1 = if x-y point is out of obj<br> 0 = if on obj<br> -1 = if in obj
     *
     * @param x
     * @param y
     * @return int
     */
    @Override
    public int isIntObjArea(float x, float y) {
        if ((getCenterX()-x)*(getCenterX()-x) + (getCenterY()-y)*(getCenterY()-y)==getRadius()*getRadius()){
            return 0;
        }
        else if ((getCenterX()-x)*(getCenterX()-x) + (getCenterY()-y)*(getCenterY()-y)<getRadius()*getRadius()){
            return -1;
        }
        return 1;
    }

    /**
     * Set radius of cycle.
     *
     * @param radius float
     */
    @Override
    public void setRadius(float radius) {
        if (radius<=0)
            throw new IllegalArgumentException("Radius of a cycle shape can not be zero or negative");
        this.radius = radius;
        getSprite().setSize(getRadius()*2, getRadius()*2);
        setMess(World.BIRD_DEFUALT_MESS*(this.radius*2));
    }

    /**
     * Get radius of cycle
     *
     * @return radius float
     */
    @Override
    public float getRadius() {
        return this.radius;
    }

    @Override
    public float getCenterX() {
        return getPositionX()+getRadius();
    }

    @Override
    public float getCenterY() {
        return getPositionY()+getRadius();
    }

    /**
     * Drow object in screen.
     */
    @Override
    public void draw() {
        Sprite s = World.setMeterSpritToPixel(getSprite());
        AngryBirdsGame.batch.draw(s, s.getX(), s.getY(), s.getOriginX(), s.getOriginY(), s.getWidth(), s.getHeight(), s.getScaleX(), s.getScaleY(), s.getRotation());
    }

    /**
     * check for know a hit happen between this shape and a rectangle shape.
     * @param rectangle
     * @return int - 0=no / 1=yes
     */
    public int checkHit(Rectangle rectangle)
    {
        return World.checkHit_CR(getCenterX(), getCenterY(), getRadius(), rectangle.getPositionX(), rectangle.getPositionY(), rectangle.getWidth(), rectangle.getHeight());
    }

    /**
     * check for know a hit happen between this shape and a Triangle shape.
     * @param triangle
     * @return int - 0=no / 1=yes
     */
    public int checkHit(Triangle triangle){
        return  World.checkHit_CT(getCenterX(), getCenterY(), getRadius(), triangle.getPositionX(), triangle.getPositionY(), triangle.getSide());
    }

    /**
     * check for know a hit happen between this shape and a Cycle shape.
     * @param cycle
     * @return int - 0=no / 1=yes
     */
    public int checkHit(Cycle cycle){
        return World.checkHit_CC(getCenterX(), getCenterY(), getRadius(), cycle.getCenterX(), cycle.getCenterY(), cycle.getRadius());
    }
}
