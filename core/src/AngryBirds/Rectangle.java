package AngryBirds;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by Ali on 01/01/2016.
 *
 * This interface describe a rectangle Object property.
 */
public interface Rectangle extends Shape {

    /**
     * Set width of rectangle.
     * @param width float
     */
    void setWidth(float width);

    /**
     * set height of rectangle
     * @param height float
     */
    void setHeight(float height);

    /**
     * Set height and width of rectangle.
     * @param width float
     * @param height float
     */
    void setAsBox(float width, float height);

    /**
     * Return width of rectangle
     * @return width float
     */
    float getWidth();

    /**
     * Return height of rectangle
     * @return height float
     */
    float getHeight();

    /**
     * get position of object in screen.
     * @return x - float
     */
    public float getPositionX();

    /**
     * get position of object in screen.
     * @return y - float
     */
    public float getPositionY();
}
