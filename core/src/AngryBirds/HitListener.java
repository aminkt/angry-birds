package AngryBirds;

import java.util.EventListener;

/**
 * Created by Ali on 24/01/2016.
 *
 * Define an action listener for perform good action to respond to a hit action.
 */
public interface HitListener extends EventListener {
    public void onHit(HitEvent e);
}
