package AngryBirds;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;

/**
 * Created by Ali on 23/01/2016.
 */
public class GlassWheel extends AbstractWheel {
    public GlassWheel(float radius) {
        setTexture("Glass Wheel 1.png");
        init(radius);
        setMess(World.GLASS_DEFUALT_MESS*(radius*2));
        setHardnessFactor(World.GLASS_DEFUALT_HARDNESS);
        setRestitution(World.GLASS_DEFUALT_RESTITUTION);


    }

    @Override
    public void setRadius(float radius) {
        super.setRadius(radius);
        setMess(World.GLASS_DEFUALT_MESS*(getRadius())+0.001f);
    }

    /**
     * <p><strong>Change shape and picture of object by them health.</strong></p>
     * <p>For example when an wood box is healthy show prefect picture for that and when is not healthy change
     * that picture to a broken box.</p>
     *
     * @param health int
     */
    @Override
    protected void setFaceType(int health) {

    }

    /**
     * <p><strong>Return a int number for describe current face type. </strong></p>
     * <p>This number is between 1 and 5.</p>
     *
     * @param health int
     * @return faceType constant int
     */
    @Override
    protected int getFaceType(int health) {
        return 0;
    }
}
