package AngryBirds;

import Cast.GameSprite;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.AngryBirdsGame;
import Cast.GameTexture;

/**
 * Created by Ali on 27/01/2016.
 *
 * For creating and handling Archery in game.
 */
public class Archery extends AbstractStaticObj implements Shape{
    private GameTexture texture2;
    private GameSprite sprite2;
    public static Vector2 centerOfArchy = new Vector2();
    public static Vector2 center = new Vector2();
    public static float[] bounds = new float[4];

    /**
     * Set texture for draw objects in game.
     * @param t String - file path
     */
    public void setTexture2(String t){
        this.texture2 = new GameTexture(t);

        sprite2 = new GameSprite(getTexture2());
    }

    public GameTexture getTexture2() {
        return  this.texture2;
    }

    public Archery(float x){
        setTexture("arch1.png");
        setTexture2("arch2.png");
        setSprit(new GameSprite(getTexture()));
        sprite2 = new GameSprite(getTexture2());
        sprite2.setSize(
                World.convertPixelToMeter(getTexture2().getTexture().getWidth()/1.5f),
                World.convertPixelToMeter(getTexture2().getTexture().getHeight()/1.5f)
        );
        getSprite().setSize(
                World.convertPixelToMeter(getTexture().getTexture().getWidth()/1.5f),
                World.convertPixelToMeter(getTexture().getTexture().getHeight()/1.5f)
        );

        setPosition(x,0);

    }

    @Override
    public void setPositionX(float x) {
        super.setPositionX(x);
        sprite2.setX(x-0.18f);
    }

    @Override
    public void setPositionY(float y) {
        super.setPositionY(Ground.getHightOfGround());
        sprite2.setY(Ground.getHightOfGround()+0.5f);
    }

    /**
     * <p><strong>Controlling hit action</strong></p>
     * <p>This method get an object and define property of object that has collision whit the current object
     * and change property of both object same forces and health of them.</p>
     * <p>Objects can be static object or movable object.</p>
     *
     * @param obj AbstractStaticObj
     */
    @Override
    public void hit(AbstractStaticObj obj) {

    }

    /**
     * check for know a hit happen between this shape and a cycle shape.
     *
     * @param cycle
     * @return int - 0=no / 1=yes
     */
    @Override
    public int checkHit(Cycle cycle) {
        return 0;
    }

    /**
     * check for know a hit happen between this shape and a triangle shape.
     *
     * @param triangle
     * @return int - 0=no / 1=yes
     */
    @Override
    public int checkHit(Triangle triangle) {
        return 0;
    }

    /**
     * check for know a hit happen between this shape and a rectangle shape.
     *
     * @param rectangle
     * @return int - 0=no / 1=yes
     */
    @Override
    public int checkHit(Rectangle rectangle) {
        return 0;
    }

    /**
     * Get an place in x-y of screen and return an int nubmer. <br> 1 = if x-y point is out of obj<br> 0 = if on obj<br> -1 = if in obj
     *
     * @param x
     * @param y
     * @return int
     */
    @Override
    public int isIntObjArea(float x, float y) {
        return 1;
    }

    public static boolean inShootRange(float x, float y){
        float r = getDeltaXOfArchery(x, y);
        if(r>65 && x<bounds[2] && x>bounds[0] && y<bounds[1] && y>bounds[3])
            return true;
        return false;
    }

    public static float getDeltaXOfArchery(float x, float y){
        return (float) Math.sqrt(Math.pow(x-center.x, 2)+Math.pow(y-center.y, 2));
    }

    /**
     * Drow object in screen.
     */
    @Override
    public void draw() {
        Sprite s = World.setMeterSpritToPixel(getSprite());
        Sprite s2 = World.setMeterSpritToPixel(sprite2);
        AngryBirdsGame.batch.draw(s, s.getX(), s.getY(), s.getWidth(), s.getHeight());
        AngryBirdsGame.batch.draw(s2, s2.getX(), s2.getY(), s2.getWidth(), s2.getHeight());
        //World.setMeterSpritToPixel(getSprite()).draw(AngryBirdsGame.batch);
        //World.setMeterSpritToPixel(sprite2).draw(AngryBirdsGame.batch);
        AngryBirdsGame.batch.end();
        AngryBirdsGame.shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        AngryBirdsGame.shapeRenderer.setColor(0.50f, 0.29f, 0.06f, 1);
        center.set(s.getX()+s.getWidth()/1.1f, s.getY()+s.getHeight()/1.19f);
        bounds[0] = s2.getX()-4*s2.getWidth();
        bounds[1] = s.getY() + s.getHeight()+50;
        bounds[2] = s.getX();
        bounds[3] = s.getY();
        float x = Gdx.input.getX();
        float y = Gdx.graphics.getHeight() - Gdx.input.getY();
        float r = (float) Math.sqrt(Math.pow(x-center.x, 2)+Math.pow(y-center.y, 2));
        if (Gdx.input.isTouched() && x> s2.getX()-4*s2.getWidth() && x<s.getX() &&
                y < s.getY() + s.getHeight()+50 &&
                y > s.getY()) {

            centerOfArchy.set(x, y);
                AngryBirdsGame.shapeRenderer.rectLine(
                        s.getX() + s.getWidth() / 1.1f,
                        s.getY() + s.getHeight() / 1.19f,
                        centerOfArchy.x,
                        centerOfArchy.y,
                        12
                );
                AngryBirdsGame.shapeRenderer.setColor(0.40f, 0.19f, 0.06f, 1);
                AngryBirdsGame.shapeRenderer.rectLine(
                        s2.getX() + s2.getWidth() / 3.5f,
                        s2.getY() + s2.getHeight() / 1.4f,
                        centerOfArchy.x,
                        centerOfArchy.y,
                        12
                );

        } else
        {
            centerOfArchy.set(center.x, center.y);
            AngryBirdsGame.shapeRenderer.rectLine(
                    s.getX()+s.getWidth()/1.1f,
                    s.getY()+s.getHeight()/1.19f,
                    centerOfArchy.x,
                    centerOfArchy.y,
                    12
            );
            AngryBirdsGame.shapeRenderer.setColor(0.40f, 0.19f, 0.06f, 1);
            AngryBirdsGame.shapeRenderer.rectLine(
                    s2.getX()+s2.getWidth()/3.5f,
                    s2.getY()+s2.getHeight()/1.4f,
                    centerOfArchy.x,
                    centerOfArchy.y,
                    12
            );
        }

        AngryBirdsGame.shapeRenderer.end();
        AngryBirdsGame.batch.begin();
    }
}
