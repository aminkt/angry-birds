package AngryBirds;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.AngryBirdsGame;

/**
 * Created by Ali on 21/01/2016.
 * <p/>
 * This abstract class define general property of a movable object in game.
 */
abstract public class AbstractMovableObj extends AbstractStaticObj {

    /**
     * use for keep align of last frame force.
     */
    Vector2 forceVector = new Vector2(0,0);


    /**
     * hold health number of object for controlling when object should be destroyed.
     */
    protected int health = 100;

    public boolean isInMoveMode(){
        if(vX == 0 && vY == 0)
            return false;
        return true;
    }

    @Override
    public boolean isStatic(){
        return false;
    }

    public void setvX(float vX) {
        this.vX = vX;
    }

    public void setvY(float vY) {
        this.vY = vY;
    }

    /**
     * Set rotate to object.
     * @param degree float
     */
    public void setRotate(float degree){
        getSprite().setRotation(degree);
    }

    /**
     * Get Rotation of object
     * @return degree - float
     */
    public float getRotate(){
        return getSprite().getRotation();
    }

    /**
     * <p><strong>Set x force and y force together</strong></p>
     *
     * @param xForce double
     * @param yForce double
     */
    public void setForce(double xForce, double yForce) {
        setXForce(xForce);
        setYForce(yForce);
    }

    /**
     * <p><strong>Set y force together</strong></p>
     *
     * @param xForce double
     */
    public synchronized void setXForce(double xForce){
        double fs;
        double time = Gdx.graphics.getDeltaTime()*World.timeFactor;
        float v;
        if(getPositionY()<= Ground.heightOfGround)
            fs = (getMess()*World.gravity* AngryBirdsGame.ground.getFriction());
        else
            fs = 0;

        if(xForce>=0)
        {
            v= (float) (((xForce+fs)*time)/getMess() +this.vX);
            if((xForce==0 || xForce<fs) && this.vX==0)
                v=0;
            if (xForce==0 && this.vX<=0)
                v=0;
        }
        else{
            v= (float) (((xForce+fs)*time)/getMess() +this.vX);
        }
        setvX(v);
    }

    /**
     * <p><strong>Set x force</strong></p>
     *
     * @param yForce double
     */
    public synchronized void setYForce(double yForce){
        double fg;
        double time = Gdx.graphics.getDeltaTime()*World.timeFactor;
        float v;
        fg = (getMess()*World.gravity);
        if(yForce>=0)
        {
            v= (float) (((yForce+fg)*time)/getMess() +this.vY);
            if(getPositionY()<= Ground.heightOfGround)
                v=0;
        }
        else{
            v= (float) (((yForce+fg)*time)/getMess() +this.vY);
        }
        setvY(v);
    }

    /**
     * This function calculate moving data and change position of object.
     */
    public synchronized void move(){
        if(getPositionY() <= Ground.heightOfGround)
            this.vY = 0;

        float time = Gdx.graphics.getDeltaTime()*World.timeFactor;
        setPositionX(getVX()*time + getPositionX());
        setPositionY(getVY()*time + getPositionY());
        setForce(0, 0);
    }
    /**
     * <p>Return health of object.</p><p>If health be 100 then object is healthy and if health be 0 then object should remove from screen.</p>
     *
     * @return health int
     */
    public int getHealth() {
        return health;
    }

    /**
     * <p><strong>Return health of object.</strong></p>
     * <p>If health be 100 then object is healthy and if health be 0 then object should remove from screen.</p>
     *
     * @param health int
     */
    public void setHealth(int health) {
        this.health = health;
    }


    /**
     * @param y float
     */
    @Override
    public void setPositionY(float y) {
        if (y < Ground.heightOfGround)
            super.setPositionY(Ground.heightOfGround);
        else
            super.setPositionY(y);

    }

    @Override
    public synchronized void hit(AbstractStaticObj obj){

        float m1 = getMess();
        float m2 = obj.getMess();
        float v1x = getVX();
        float v1y = getVY();
        float v2x = obj.getVX();
        float v2y = obj.getVY();



        //System.err.println(Math.sqrt(Math.pow(obj.getVX(), 2) + Math.pow(obj.getVY(), 2)) + " | "+ this.getHardnessFactor());

        if(Math.sqrt(Math.pow(obj.getVX(), 2) + Math.pow(obj.getVY(), 2))>this.getHardnessFactor())
            AngryBirdsGame.holder.remove(this);


        //if(Math.sqrt(Math.pow(e.getObj1().getVX(), 2) + Math.pow(e.getObj1().getVY(), 2))>e.getObj1().getHardnessFactor())
        //    AngryBirdsGame.holder.remove(e.getObj2());


        float vex = (m1*v1x - m2*v2x)/(m1+m2);
        float vey = (m1*v1y - m2*v2y)/(m1+m2);
        setvX(vex*getRestitution());
        obj.setvX(vex*getRestitution());
        setvY(vey*getRestitution());
        obj.setvY(vey*getRestitution());
    }

    /**
     * <p><strong>Change shape and picture of object by them health.</strong></p>
     * <p>For example when an wood box is healthy show prefect picture for that and when is not healthy change
     * that picture to a broken box.</p>
     * @param health int
     */
    protected abstract void setFaceType(int health);


    /**
     * <p><strong>Return a int number for describe current face type. </strong></p>
     * <p>This number is between 1 and 5.</p>
     * @param health int
     * @return faceType constant int
     */
    protected abstract int getFaceType(int health);

}
