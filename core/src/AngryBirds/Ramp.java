package AngryBirds;

/**
 * Created by Ali on 30/01/2016.
 */
public class Ramp extends AbstractStaticObj implements Triangle {

    protected float side;
    public Ramp(float side){
        this.side = side;
        setMess(World.STONE_DEFUALT_MESS*(side*side));
        setHardnessFactor(World.STONE_DEFUALT_HARDNESS);
        setRestitution(World.STONE_DEFUALT_RESTITUTION);


    }

    public void setSide(float side) {
        if(side<=0)
            throw new IllegalArgumentException("Side can not be zero or negative number.");
        this.side = side;
        setMess(World.STONE_DEFUALT_MESS*(getSide()*getSide())+0.001f);
    }

    /**
     * Get an place in x-y of screen and return an int nubmer. <br> 1 = if x-y point is out of obj<br> 0 = if on obj<br> -1 = if in obj
     *
     * @param x
     * @param y
     * @return int
     */
    @Override
    public int isIntObjArea(float x, float y) {
        //INPUT:    (xt, yt) mokhtasate gosheye chap paeen, a ضلع, (x,y) checkpoint
        //OUTPUT:   1: biroon   0:roo khat  -1:dakhel
        double b= Math.sqrt(3);

        if  (CheckinRectangular(getPositionX(), getPositionY() , side, side , x , y)<=0)
        {
            if (y == x+ getPositionY()- getPositionX() || y==getPositionY() || x==getPositionX()+side)
                return 0;
            else if (y<x+getPositionY()-getPositionX())
                return -1;
            else
                return 1;
        }
        else
            return 1;
    }

    /**
     * Drow object in screen.
     */
    @Override
    public void draw() {

    }

    /**
     * Return Triangle side.
     *
     * @return
     */
    @Override
    public float getSide() {
        return side;
    }


    /**
     * check for know a hit happen between this shape and a Rectangle shape.
     * @param rectangle
     * @return int - 0=no / 1=yes
     */
    public int checkHit(Rectangle rectangle)
    {
        return World.checkHit_TR(getPositionX(), getPositionY(), getSide(), rectangle.getPositionX(), rectangle.getPositionY(), rectangle.getWidth(), rectangle.getHeight());
    }

    /**
     * <p><strong>Controlling hit action</strong></p>
     * <p>This method get an object and define property of object that has collision whit the current object
     * and change property of both object same forces and health of them.</p>
     * <p>Objects can be static object or movable object.</p>
     *
     * @param obj AbstractStaticObj
     */
    @Override
    public void hit(AbstractStaticObj obj) {

    }

    /**
     * check for know a hit happen between this shape and a Cycle shape.
     * @param cycle
     * @return int - 0=no / 1=yes
     */
    public int checkHit(Cycle cycle){
        return World.checkHit_CT(cycle.getCenterX(), cycle.getCenterY(), cycle.getRadius(), getPositionX(), getPositionY(), getSide());
    }

    /**
     * check for know a hit happen between this shape and a Triangle shape.
     * @param triangle
     * @return int - 0=no / 1=yes
     */
    public int checkHit(Triangle triangle){
        return World.checkHit_TT(getPositionX(), getPositionY(), getSide(), triangle.getPositionX(), triangle.getPositionY(), triangle.getSide());
    }
}
