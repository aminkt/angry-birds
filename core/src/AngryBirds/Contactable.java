package AngryBirds;

import com.badlogic.gdx.math.Vector2;

import java.io.Serializable;

/**
 * This interface is root of inherit tree for creating AngryBirds Objects.
 */
public interface Contactable extends Serializable {


    /**
     * Get an place in x-y of screen and return an int nubmer. <br> 1 = if x-y point is out of obj<br> 0 = if on obj<br> -1 = if in obj
     * @param x
     * @param y
     * @return int
     */
    int isIntObjArea(float x, float y);

    /**
     * Calculate and return a boolean to describe status of to Shape object.<br>
     *     return false when no contact happened and return true when two object has contact together.
     * @param obj Shape -> shape two
     * @return boolean
     */
    boolean checkContact(AbstractStaticObj obj);

    /**
     * Drow object in screen.
     */
    void draw();
}
