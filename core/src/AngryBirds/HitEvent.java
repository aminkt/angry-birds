package AngryBirds;

import com.badlogic.gdx.math.Vector2;
import java.util.EventObject;

/**
 * Created by Ali on 24/01/2016.
 *
 * Define an Event for detect hit between two object.
 */
public class HitEvent extends EventObject{
    public static final int LEFT = 0;
    public static final int Right = 0;
    public static final int Top = 0;
    public static final int BOTTOM = 0;


    protected AbstractStaticObj obj1;
    protected AbstractStaticObj obj2;
    protected Vector2 vOfObject1;
    protected Vector2 vOfObject2;

    /**
     * Constructs a prototypical Event.
     *
     * @param object1 The object on which the Event initially occurred.
     * @throws IllegalArgumentException if source is null.
     */
    public HitEvent(Object source, AbstractStaticObj object1, AbstractStaticObj object2) {
        super(source);
        obj1 = object1;
        obj2 = object2;
        vOfObject1 = new Vector2(object1.vX, object2.vY);
        vOfObject2 = new Vector2(object2.vX, object2.vY);
    }

    /**
     * Return object that hit other object.
     * @return shape
     */
    public AbstractStaticObj getObj1() {
        return obj1;
    }

    /**
     * Return hited Object.
     * @return
     */
    public AbstractStaticObj getObj2() {
        return obj2;
    }

    /**
     * Return current force of first object.
     * @return
     */
    public Vector2 getForceOfFirstObj() {
        return vOfObject1;
    }

    /**
     * Return current force of second object.
     * @return
     */
    public Vector2 getForceOfSecondObj() {
        return vOfObject2;
    }
}
