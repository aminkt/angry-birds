package AngryBirds;

import Cast.GameSprite;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.AngryBirdsGame;

/**
 * Created by Ali on 20/01/2016.
 * This class define Walls behavior for do good reaction to objects in game.
 * ground is an static obj and don't move by force but same as other object has its physics.
 */
public class Wall extends AbstractStaticObj implements Rectangle{

    private float width;
    private float height;

    /**
     * Create a wall instance in game.
     * @param width
     * @param height
     */
    public Wall(float width, float height){
        setFriction(World.STONE_DEFUALT_FRICTION);
        setRestitution(World.STONE_DEFUALT_RESTITUTION);

        setTexture("wall.png");
        setSprit(new GameSprite(getTexture()));

        setWidth(width);
        setHeight(height);

        setMess(World.STONE_DEFUALT_MESS*(width*height));
        setHardnessFactor(World.STONE_DEFUALT_HARDNESS);

    }
    /**
     * Create a wall instance in game
     * @param width
     * @param height
     * @param friction
     */
    public Wall(float width, float height, float friction){
        setWidth(width);
        setHeight(height);
        setFriction(friction);
        setRestitution(World.STONE_DEFUALT_RESTITUTION);
        setHardnessFactor(World.STONE_DEFUALT_HARDNESS);

    }

    /**
     * Create a wall instance in game.
     * @param width
     * @param height
     * @param friction
     * @param restitution
     */
    public Wall(float width, float height, float friction, float restitution){
        setWidth(width);
        setHeight(height);
        setFriction(friction);
        setRestitution(restitution);
        setHardnessFactor(World.STONE_DEFUALT_HARDNESS);

    }
    /**
     * Set width of rectangle.
     *
     * @param width float
     */
    @Override
    public void setWidth(float width) {
        if (width<=0)
            throw new IllegalArgumentException("Width of box can not be zero or negative number.");
        this.width = width;
        getSprite().setSize(getWidth(),getHeight());
        setMess(World.STONE_DEFUALT_MESS*(getWidth()*getHeight())+0.001f);
    }

    /**
     * set height of rectangle
     *
     * @param height float
     */
    @Override
    public void setHeight(float height) {
        if (height<=0)
            throw new IllegalArgumentException("Height of box can not be zero or negative number.");
        this.height = height;
        getSprite().setSize(getWidth(),getHeight());

        setMess(World.STONE_DEFUALT_MESS*(getWidth()*getHeight())+0.001f);
    }

    /**
     * Set height and width of rectangle.
     *
     * @param width  float
     * @param height float
     */
    @Override
    public void setAsBox(float width, float height) {
        this.width = width;
        this.height = height;
    }

    /**
     * Return width of rectangle
     *
     * @return width float
     */
    @Override
    public float getWidth() {
        return this.width;
    }

    /**
     * Return height of rectangle
     *
     * @return height float
     */
    @Override
    public float getHeight() {
        return this.height;
    }


    /**
     * @param y float
     */
    @Override
    public void setPositionY(float y) {
        if (y < Ground.heightOfGround)
            super.setPositionY(Ground.heightOfGround);
        else
            super.setPositionY(y);
    }

    /**
     * <p><strong>Controlling hit action</strong></p>
     * <p>This method get an object and define property of object that has collision whit the current object
     * and change property of both object same forces and health of them.</p>
     * <p>Objects can be static object or movable object.</p>
     *
     * @param obj AbstractStaticObj
     */
    @Override
    public synchronized void hit(AbstractStaticObj obj) {
        if(obj.getPositionX()+obj.getSprite().getWidth() > getPositionX() && obj.getVX()>0){
            obj.setPositionX(getPositionX() - obj.getSprite().getWidth());
        } else if (obj.getPositionX()< getPositionX()+getWidth() && obj.getVX()<0){
            obj.setPositionX(getPositionX()+getWidth());
        }

        if(obj.getPositionY() < getPositionY()+getHeight() && obj.getVY()<0){
            obj.setPositionY(getPositionY()+getHeight());
        } else if (obj.getPositionY()+obj.getSprite().getHeight()< getPositionY() && obj.getVX()<0){
            obj.setPositionX(getPositionY());
        }
        obj.setvX(-obj.getVX()*getRestitution()*obj.getRestitution());
        obj.setvY(-obj.getVY()*getRestitution()*obj.getRestitution());
    }


    /**
     * Drow object in screen.
     */
    @Override
    public void draw() {
        Sprite s = World.setMeterSpritToPixel(getSprite());
        AngryBirdsGame.batch.draw(s, s.getX(), s.getY(), s.getWidth(), s.getHeight());
    }

    /**
     * Get an place in x-y of screen and return an int nubmer. <br> 1 = if x-y point is out of obj<br> 0 = if on obj<br> -1 = if in obj
     *
     * @param x
     * @param y
     * @return int
     */
    @Override
    public int isIntObjArea(float x, float y) {
        if (getSprite().getX()<= x && x <= getSprite().getX()+getWidth() && getSprite().getY()<= y && y <= getSprite().getY()+getHeight() )
        {
            if (x==getSprite().getX() || x==getSprite().getX()+getWidth() || y==getSprite().getY() || y==getSprite().getY()+getHeight() )
                return 0;
            else
                return -1;
        }
        return 1;
    }

    /**
     * check for know a hit happen between this shape and a cycle shape.
     * @param cycle
     * @return int - 0=no / 1=yes
     */
    public int checkHit(Cycle cycle)
    {
        return World.checkHit_CR(cycle.getCenterX(), cycle.getCenterY(), cycle.getRadius(), getPositionX(), getPositionY(), getWidth(), getHeight());
    }

    /**
     * check for know a hit happen between this shape and a triangle shape.
     * @param triangle
     * @return int - 0=no / 1=yes
     */
    public int checkHit(Triangle triangle)
    {
        return World.checkHit_TR(triangle.getPositionX(), triangle.getPositionY(), triangle.getSide(), getPositionX(), getPositionY(), getWidth(), getHeight());
    }

    /**
     * check for know a hit happen between this shape and a rectangle shape.
     * @param rectangle
     * @return int - 0=no / 1=yes
     */
    public int checkHit(Rectangle rectangle){
        return World.checkHit_RR(getPositionX(), getPositionY(), getWidth(), getHeight(), rectangle.getPositionX(), rectangle.getPositionY(), rectangle.getWidth(), rectangle.getHeight());
    }
}
