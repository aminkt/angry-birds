package AngryBirds;

import Cast.GameSprite;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.mygdx.game.AngryBirdsGame;

/**
 * Created by Ali on 22/01/2016.
 *
 * Describe Pig Object property.
 */
public class Pig extends AbstractMovableObj implements Cycle {

    protected float radius;

    public Pig(float radius) {
        setTexture("pig1.png");
        super.setSprit(new GameSprite(getTexture()));
        setRadius(radius);
        setMess(World.PIG_DEFUALT_MESS*(radius));
        setHardnessFactor(World.PIG_DEFUALT_HARDNESS);
        setRestitution(World.PIG_DEFUALT_RESTITUTION);
    }


    /**
     * <p><strong>Controlling hit action</strong></p>
     * <p>This method get an object and define property of object that has collision whit the current object
     * and change property of both object same forces and health of them.</p>
     * <p>Objects can be static object or movable object.</p>
     *
     * @param obj AbstractStaticObj
     */
    @Override
    public void hit(AbstractStaticObj obj) {
        super.hit(obj);
    }

    /**
     * <p><strong>Change shape and picture of object by them health.</strong></p>
     * <p>For example when an wood box is healthy show prefect picture for that and when is not healthy change
     * that picture to a broken box.</p>
     *
     * @param health int
     */
    @Override
    protected void setFaceType(int health) {

    }

    /**
     * <p><strong>Return a int number for describe current face type. </strong></p>
     * <p>This number is between 1 and 5.</p>
     *
     * @param health int
     * @return faceType constant int
     */
    @Override
    protected int getFaceType(int health) {
        return 0;
    }

    /**
     * Set radius of cycle.
     *
     * @param radius float
     */
    @Override
    public void setRadius(float radius) {
        if (radius<=0)
            throw new IllegalArgumentException("Radius of a cycle shape can not be zero or negative");
        this.radius = radius;
        getSprite().setSize(getRadius()*2, getRadius()*2);
        setMess(World.PIG_DEFUALT_MESS*(getRadius()*2)+0.001f);
    }

    @Override
    public float getCenterX() {
        return getPositionX()+getRadius();
    }

    @Override
    public float getCenterY() {
        return getPositionY()+getRadius();
    }

    /**
     * Get radius of cycle
     *
     * @return radius float
     */
    @Override
    public float getRadius() {
        return this.radius;
    }

    /**
     * Drow object in screen.=
     */
    @Override
    public void draw() {
        Sprite s = World.setMeterSpritToPixel(getSprite());
        AngryBirdsGame.batch.draw(s, s.getX(), s.getY(), s.getWidth(), s.getHeight());
    }

    /**
     * Get an place in x-y of screen and return an int nubmer. <br> 1 = if x-y point is out of obj<br> 0 = if on obj<br> -1 = if in obj
     *
     * @param x
     * @param y
     * @return int
     */
    @Override
    public int isIntObjArea(float x, float y){
        if ((getCenterX()-x)*(getCenterX()-x) + (getCenterY()-y)*(getCenterY()-y)==getRadius()*getRadius()){
            return 0;
        }
        else if ((getCenterX()-x)*(getCenterX()-x) + (getCenterY()-y)*(getCenterY()-y)<getRadius()*getRadius()){
            return -1;
        }
        return 1;
    }

    /**
     * check for know a hit happen between this shape and a rectangle shape.
     * @param rectangle
     * @return int - 0=no / 1=yes
     */
    public int checkHit(Rectangle rectangle)
    {
        return World.checkHit_CR(getCenterX(), getCenterY(), getRadius(), rectangle.getPositionX(), rectangle.getPositionY(), rectangle.getWidth(), rectangle.getHeight());
    }

    /**
     * check for know a hit happen between this shape and a Triangle shape.
     * @param triangle
     * @return int - 0=no / 1=yes
     */
    public int checkHit(Triangle triangle){
        return  World.checkHit_CT(getCenterX(), getCenterY(), getRadius(), triangle.getPositionX(), triangle.getPositionY(), triangle.getSide());
    }

    /**
     * check for know a hit happen between this shape and a Cycle shape.
     * @param cycle
     * @return int - 0=no / 1=yes
     */
    public int checkHit(Cycle cycle){
        return World.checkHit_CC(getCenterX(), getCenterY(), getRadius(), cycle.getCenterX(), cycle.getCenterY(), cycle.getRadius());
    }
}
