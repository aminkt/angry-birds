package AngryBirds;

/**
 * Created by Ali on 01/01/2016.
 *
 * This interface describe a cycle Object property.
 */
public interface Cycle extends Shape{

    /**
     * Set radius of cycle.
     * @param radius float
     */
    void setRadius(float radius);

    /**
     * Get radius of cycle
     * @return radius float
     */
    float getRadius();

    /**
     * Return position of center of cycle in x straight
     * @return x float
     */
    float getCenterX();

    /**
     * Return position of center of cycle in y straight
     * @return y float
     */
    float getCenterY();

    /**
     * get position of object in screen.
     * @return x - float
     */
    public float getPositionX();

    /**
     * get position of object in screen.
     * @return y - float
     */
    public float getPositionY();
}
