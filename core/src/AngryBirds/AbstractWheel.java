package AngryBirds;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.mygdx.game.AngryBirdsGame;
import Cast.GameSprite;

/**
 * Created by Ali on 23/01/2016.
 */
public abstract class AbstractWheel extends AbstractMovableObj implements Cycle {

    protected float radius;

    /**
     * For reduce retyping code of child class init method, work same this abstract constructor.
     * @param radius float
     */
    public void init(float radius){
        setSprit(new GameSprite(getTexture()));
        setRadius(radius);
        //getSprite().setSize(getRadius()*2, getRadius()*2);
    }


    /**
     * <p><strong>Controlling hit action</strong></p>
     * <p>This method get an object and define property of object that has collision whit the current object
     * and change property of both object same forces and health of them.</p>
     * <p>Objects can be static object or movable object.</p>
     *
     * @param obj AbstractStaticObj
     */
    @Override
    public void hit(AbstractStaticObj obj) {
        super.hit(obj);
    }

    public synchronized void move() {
        super.move();
        float d = getRotate();
        setRotate(-getVX()*4+d);
    }

    /**
     * Set radius of cycle.
     *
     * @param radius float
     */
    @Override
    public void setRadius(float radius) {
        if (radius<=0)
            throw new IllegalArgumentException("Radius can not be zero or negative number.");
        this.radius = radius;
        getSprite().setSize(getRadius()*2, getRadius()*2);
    }

    /**
     * Get radius of cycle
     *
     * @return radius float
     */
    @Override
    public float getRadius() {
        return this.radius;
    }

    @Override
    public float getCenterX() {
        return getPositionX()+getRadius();
    }

    @Override
    public float getCenterY() {
        return getPositionY()+getRadius();
    }

    /**
     * Drow object in screen.
     */



    @Override
    public void draw() {
        Sprite s = World.setMeterSpritToPixel(getSprite());
        //AngryBirdsGame.batch.draw(s, s.getX(), s.getY(), s.getWidth(), s.getHeight());
        AngryBirdsGame.batch.draw(s, s.getX(), s.getY(), s.getOriginX(), s.getOriginY(), s.getWidth(), s.getHeight(), s.getScaleX(), s.getScaleY(), s.getRotation());
    }

    /**
     * Get an place in x-y of screen and return an int nubmer. <br> 1 = if x-y point is out of obj<br> 0 = if on obj<br> -1 = if in obj
     *
     * @param x
     * @param y
     * @return int
     */
    @Override
    public int isIntObjArea(float x, float y) {
        if ((getCenterX()-x)*(getCenterX()-x) + (getCenterY()-y)*(getCenterY()-y)==getRadius()*getRadius()){
            return 0;
        }
        else if ((getCenterX()-x)*(getCenterX()-x) + (getCenterY()-y)*(getCenterY()-y)<getRadius()*getRadius()){
            return -1;
        }
        return 1;
    }

    /**
     * check for know a hit happen between this shape and a rectangle shape.
     * @param rectangle
     * @return int - 0=no / 1=yes
     */
    public int checkHit(Rectangle rectangle)
    {
        return World.checkHit_CR(getCenterX(), getCenterY(), getRadius(), rectangle.getPositionX(), rectangle.getPositionY(), rectangle.getWidth(), rectangle.getHeight());
    }

    /**
     * check for know a hit happen between this shape and a Triangle shape.
     * @param triangle
     * @return int - 0=no / 1=yes
     */
    public int checkHit(Triangle triangle){
        return  World.checkHit_CT(getCenterX(), getCenterY(), getRadius(), triangle.getPositionX(), triangle.getPositionY(), triangle.getSide());
    }

    /**
     * check for know a hit happen between this shape and a Cycle shape.
     * @param cycle
     * @return int - 0=no / 1=yes
     */
    public int checkHit(Cycle cycle){
        return World.checkHit_CC(getCenterX(), getCenterY(), getRadius(), cycle.getCenterX(), cycle.getCenterY(), cycle.getRadius());
    }
}
