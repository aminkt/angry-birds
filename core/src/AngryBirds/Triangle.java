package AngryBirds;

/**
 * Created by Ali on 01/01/2016.
 *
 * This interface describe a Triangle Object property.
 */
public interface Triangle extends Shape {
    /**
     * get position of object in screen.
     * @return x - float
     */
    public float getPositionX();

    /**
     * get position of object in screen.
     * @return y - float
     */
    public float getPositionY();


    /**
     * Return Triangle side.
     * @return
     */
    public float getSide();
}
