package AngryBirds;

import Cast.GameSprite;
import Cast.GameTexture;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;

import java.util.LinkedList;

/**
 * Created by Ali on 24/12/2015..
 */
public abstract class AbstractStaticObj implements Contactable {
    /**
     * Define a texture for saving object picture for view in screen.
     */
    protected GameTexture texture;
    //protected String texture;
    /**
     * Define an sprite object for saving position an object and draw it in screen.
     */
    protected GameSprite sprite;
    /**
     * Define mess property of object for calculate moving behavior.
     */
    protected float mess;
    /**
     * Define friction property of object for calculate moving behavior.
     */
    protected float friction;
    /**
     * Define restitution property of object for calculate reaction force.
     */
    protected float restitution;
    /**
     * define hardness factor for calculate the range of damage that a object get when hit by other object.
     */
    protected float hardnessFactor;

    public boolean isDrag = false;
    public boolean isResize = false;
    public boolean inChangePropertyMode = false;


    /**
     * save obj region.
     */
    public LinkedList<Vector2> points;

    /**
     * Return a boolean to show object is static or not
     * @return boolean
     */
    public boolean isStatic(){
        return true;
    }
    /**
     * set Sprite for get obj position in every frame.
     * @param sprite
     */
    public void setSprit(GameSprite sprite){
        this.sprite = sprite;
    }

    /**
     * Set texture for draw objects in game.
     * @param t String - file path
     */
    public void setTexture(String t){
        this.texture = new GameTexture(t);

        setSprit(new GameSprite(getTexture()));
    }

    public GameTexture getTexture() {
        return  this.texture;
    }

    /**
     * return Object sprite for get position and rotation for this time
     * @return
     */
    public GameSprite getSprite() {
        return sprite;
    }

    /**
     * return object <b>mess</b> for physics equations.
     * @return
     */
    public float getMess() {
        return mess;
    }

    /**
     * use for keep align of last frame force.
     */
    Vector2 forceVector = new Vector2(0,0);
    /**
     * hold velocity of object in x straight.
     */
    protected float vX = 0;
    /**
     * hold velocity of object in Y straight.
     */
    protected float vY = 0;



    public float getVX(){
        return  this.vX;
    }

    public float getVY(){
        return this.vY;
    }

    public void setvX(float vX) {
        this.vX = vX;
    }

    public void setvY(float vY) {
        this.vY = vY;
    }
    /**
     * set <b>mess</b> for physics equations.
     * @param mess
     */
    public void setMess(float mess) {
        if (mess<=0)
            throw new IllegalArgumentException("Mess can not be zero or negative.");
        this.mess = mess;
    }

    /**
     * return object <b>friction</b> for physics equations.
     * @return
     */
    public float getFriction() {
        return friction;
    }

    /**
     * set <b>friction</b> for physics equations.
     * @param friction
     */
    public void setFriction(float friction) {
        if (friction<0)
            throw new IllegalArgumentException("Friction can not be negative.");
        this.friction = friction;
    }

    /**
     * return object <b>restitution</b> for physics equations.
     * restitution define object reaction when that contact with another object.
     * @return
     */
    public float getRestitution() {
        return restitution;
    }

    /**
     * set <b>restitution</b> for physics equations.
     * @param restitution
     */
    public void setRestitution(float restitution) {
        this.restitution = restitution;
    }

    /**
     * set x position of object for physics and rendering.
     * @param x
     */
    public void setPositionX(float x){
        this.sprite.setX(x);
    }

    /**
     * set y position of object for physics and rendering.
     * @param y
     */
    public void setPositionY(float y){
        this.sprite.setY(y);
    }

    /**
     * set position of object for physics and rendering.
     * @param x
     * @param y
     */
    public void setPosition(float x, float y){
        this.setPositionX(x);
        this.setPositionY(y);
    }




    /**
     * get position of object in screen.
     * @return x - float
     */
    public float getPositionX(){
        //return World.convertPixelToMeter(this.sprite.getX());
        return this.sprite.getX();
    }

    /**
     * get position of object in screen.
     * @return y - float
     */
    public float getPositionY(){
        //return World.convertPixelToMeter(this.sprite.getY());
        return this.sprite.getY();
    }

    public float getPositionXinPixel(){
        return World.convertMeterToPixel(this.sprite.getX());
    }

    public float getPositionYinPixel(){
        return World.convertMeterToPixel(this.sprite.getY());
    }

    /**
     * get position of object in screen.
     * @return position Vector2
     */
    public Vector2 getPosition(){
        return new Vector2(this.getPositionX(), this.getPositionY());
    }



    /**
     * <P><strong>Return hardness factor</strong></P>
     * <p>Hardness factor use for calculate how much of object health should reduce from object's health.</p>
     *
     * @return hardnessFactor - float
     */
    public float getHardnessFactor() {
        return hardnessFactor*getMess();
    }

    /**
     * <p><strong>Controlling hit action</strong></p>
     * <p>This method get an object and define property of object that has collision whit the current object
     * and change property of both object same forces and health of them.</p>
     * <p>Objects can be static object or movable object.</p>
     *
     * @param obj AbstractStaticObj
     */
    public abstract void hit(AbstractStaticObj obj);
    /**
     * <p><strong>Set hardness factor</strong></p>
     * <p>Hardness factor use for calculate how much of object health should reduce from object's health.</p>
     *
     * @param hardnessFactor float
     */
    public void setHardnessFactor(float hardnessFactor) {
        this.hardnessFactor = hardnessFactor;
    }



    static int CheckinCircle(double xc, double yc, double r, double x, double y)
    {
        //INPUTS:   (xc,yc) mokhtasate markaz, r shoae, (x,y) checkpoint
        //OUTPUT:   1: biroon   0:roo khat  -1:dakhel
        if ((x-xc)*(x-xc) + (y-yc)*(y-yc)==r*r){
            return 0;
        }
        else if ((x-xc)*(x-xc) + (y-yc)*(y-yc)<r*r){
            return -1;
        }
        else
            return 1;
    }

    static int CheckinRectangular(double xr , double yr, double w, double h, double x, double y)
    {
        //INPUT:    (xr,yr) mokhtasate point chap paeen, w arz , h ertefa, (x,y) checkpoint
        //OUTPUT:   1: biroon   0:roo khat  -1:dakhel
        if (xr<= x && x <= xr+w && yr<= y && y <= yr+h )
        {
            if (x==xr || x==xr+w || y==yr || y==yr+h )
                return 0;
            else
                return -1;
        }
        else
            return 1;
    }
    // مثلث قائم الزاویه با زاویه چهل و پنج و ضلع آ
    static int CheckinTriangle(double xt, double yt, double a , double x, double y)
    {
        //INPUT:    (xt, yt) mokhtasate gosheye chap paeen, a ضلع, (x,y) checkpoint
        //OUTPUT:   1: biroon   0:roo khat  -1:dakhel
        double b= Math.sqrt(3);

        if  (CheckinRectangular(xt, yt , a, a , x , y)<=0)
        {
            if (y == x+ yt- xt || y==yt || x==xt+a)
                return 0;
            else if (y<x+yt-xt)
                return -1;
            else
                return 1;
        }
        else
            return 1;
    }

    /**
     * check for know a hit happen between this shape and a cycle shape.
     * @param cycle
     * @return int - 0=no / 1=yes
     */
    public abstract int checkHit(Cycle cycle);

    /**
     * check for know a hit happen between this shape and a triangle shape.
     * @param triangle
     * @return int - 0=no / 1=yes
     */
    public abstract int checkHit(Triangle triangle);

    /**
     * check for know a hit happen between this shape and a rectangle shape.
     * @param rectangle
     * @return int - 0=no / 1=yes
     */
    public abstract int checkHit(Rectangle rectangle);

    public boolean checkContact(AbstractStaticObj obj){

        if(obj instanceof Cycle)
            return this.checkHit((Cycle) obj)==1;
        else if(obj instanceof Triangle)
            return this.checkHit((Triangle) obj)==1;
        else if(obj instanceof Rectangle)
            return this.checkHit((Rectangle) obj)==1;

        return false;
    }
}
