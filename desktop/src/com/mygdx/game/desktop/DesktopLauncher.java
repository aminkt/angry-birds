package com.mygdx.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.mygdx.game.AngryBirdsGame;

public class DesktopLauncher {
	public final static int SCREEN_WIDTH = 1640;
	public final static int SCREEN_HEIGHT = 914;


	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width= 1640;
		config.height = 914;
		config.title = "Angry Birds Game";
		//config.fullscreen =true;
		new LwjglApplication(new AngryBirdsGame(), config);
	}
}
